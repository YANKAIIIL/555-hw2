package edu.upenn.cis.cis455.crawler;

import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class CrawlerTest {
    private String path = "./test";
    private String url = "https://crawltest.cis.upenn.edu/cnn/cnn_us.rss.xml";
    private StorageInterface dbMock = null;

    @Before
    public void setup(){
        if (!Files.exists(Paths.get(path))) {
            try {
                Files.createDirectory(Paths.get(path));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Test
    public void crawlerTest1(){
        //count is 100
        dbMock = StorageFactory.getDatabaseInstance(path);
        Crawler crawler = new Crawler(url, dbMock, 1048576, 100);
        crawler.start();
        assertEquals(dbMock.getCorpusSize(), 1);
    }

    @Test
    public void crawlerTest2(){
        //set size to 0, no file will be crawled. The result is still 1, which
        //was fetched in test1
        path = "https://crawltest.cis.upenn.edu/bbc/frontpage.xml";
        dbMock = StorageFactory.getDatabaseInstance(path);
        Crawler crawler = new Crawler(url, dbMock, 0, 100);
        crawler.start();
        assertEquals(dbMock.getCorpusSize(), 1);
    }
}

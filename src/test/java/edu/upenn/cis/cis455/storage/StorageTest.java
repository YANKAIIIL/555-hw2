package edu.upenn.cis.cis455.storage;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.data.URLEntity;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class StorageTest {
    private String path = "./test_storage";
    private StorageInterface dbMock = null;

    @Before
    public void setup(){
        if (!Files.exists(Paths.get(path))) {
            try {
                Files.createDirectory(Paths.get(path));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        dbMock = StorageFactory.getDatabaseInstance(path);
    }

    @Test
    public void storageTest1(){
        //count is 100
        String key = dbMock.addDocument("abc", "abc");
        dbMock.addURL(new URLEntity("url", key, 0L));
        assertEquals(dbMock.getDocument("url"), "abc");
    }

    @Test
    public void storageTest2(){
        //set size to 0, no file will be crawled
        assertEquals(1, dbMock.getCorpusSize());
    }
}

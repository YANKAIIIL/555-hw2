package edu.upenn.cis.cis455.enums;

public enum Page {
    LOGIN_FORM("login-form.html"),
    REGISTER("register.html"),
    REGISTER_ALL_FIELDS("register-all-fields.html");


    private String value;

    Page(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}

package edu.upenn.cis.cis455.enums;

public enum Table {
    USER_TABLE("user-table"),
    DOC_TABLE("doc-table"),
    CLASS_TABLE("class-table"),
    URL_TABLE("url-table");


    private String value;

    Table(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}

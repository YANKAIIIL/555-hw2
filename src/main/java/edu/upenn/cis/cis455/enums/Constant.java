package edu.upenn.cis.cis455.enums;

public enum Constant {
    HTTPS("https://"),
    HTTP("http://"),
    TEXT_HTML("text/html"),
    TEXT_XML("text/xml"),
    APPLICATION_XML("application/xml"),
    RSS_END("+xml");

    private String value;

    Constant(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}

package edu.upenn.cis.cis455.data;

import java.io.Serializable;

/**
 * Data model for URL table
 */
public class URLEntity implements Serializable {
    private String url;
    private String id;
    private long timestamp;

    public URLEntity(String url, String id, long timestamp) {
        this.url = url;
        this.id = id;
        this.timestamp = timestamp;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}

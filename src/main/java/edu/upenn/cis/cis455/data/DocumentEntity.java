package edu.upenn.cis.cis455.data;

import java.io.Serializable;

/**
 * Data model for document table
 */
public class DocumentEntity implements Serializable {
    private String id;
    private String content;
    private String docType;

    public DocumentEntity(String id, String content, String docType) {
        this.id = id;
        this.content = content;
        this.docType = docType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }
}

package edu.upenn.cis.cis455.storage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StorageFactory {
    private static Logger logger = LogManager.getLogger(StorageFactory.class);
    private static volatile StorageInterface _db = null;

    public static StorageInterface getDatabaseInstance(String directory) {
        // TODO: factory object, instantiate your storage server
        //web crawler and web server are sharing the same db
        if (_db == null) {
            synchronized (StorageFactory.class) {
                if (_db == null) {
                    _db = new StorageImpl(directory);
                }
            }
        }
        return _db;
    }
}

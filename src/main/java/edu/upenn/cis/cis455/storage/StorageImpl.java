package edu.upenn.cis.cis455.storage;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.crawler.utils.URLParser;
import edu.upenn.cis.cis455.data.DocumentEntity;
import edu.upenn.cis.cis455.data.URLEntity;
import edu.upenn.cis.cis455.data.UserEntity;
import edu.upenn.cis.cis455.enums.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.security.MessageDigest;
import java.util.SortedMap;

public class StorageImpl implements StorageInterface {

    private static Logger logger = LogManager.getLogger(StorageImpl.class);
    private Environment environment;
    EnvironmentConfig environmentConfig;

    private DatabaseConfig databaseConfig;

    private Database catalogDao;
    StoredClassCatalog catalog;

    private Database docDao;
    SortedMap<String, DocumentEntity> docTable;

    private Database userDao;
    SortedMap<String, UserEntity> userTable;

    private Database urlDao;
    SortedMap<String, URLEntity> urlTable;

    /**
     * Initialize the whole database.
     * All the tables were initialized separately
     */
    StorageImpl(String directory) {
        autoconfig(directory);
        catalogDaoConfig();
        userDaoConfig();
        docDaoConfig();
        urlDaoConfig();
    }

    private void urlDaoConfig() {
        TupleBinding<String> key = TupleBinding.getPrimitiveBinding(String.class);
        EntryBinding<URLEntity> value = new SerialBinding<>(catalog, URLEntity.class);
        urlDao = environment.openDatabase(null, Table.URL_TABLE.value(), databaseConfig);
        urlTable = new StoredSortedMap<>(urlDao, key, value, true);
    }

    private void docDaoConfig() {
        TupleBinding<String> key = TupleBinding.getPrimitiveBinding(String.class);
        EntryBinding<DocumentEntity> value = new SerialBinding<>(catalog, DocumentEntity.class);
        docDao = environment.openDatabase(null, Table.DOC_TABLE.value(), databaseConfig);
        docTable = new StoredSortedMap<>(docDao, key, value, true);
    }

    private void userDaoConfig() {
        TupleBinding<String> key = TupleBinding.getPrimitiveBinding(String.class);
        EntryBinding<UserEntity> value = new SerialBinding<>(catalog, UserEntity.class);
        userDao = environment.openDatabase(null, Table.USER_TABLE.value(), databaseConfig);
        userTable = new StoredSortedMap<>(userDao, key, value, true);
    }

    private void catalogDaoConfig() {
        catalogDao = environment.openDatabase(null, Table.CLASS_TABLE.value(), databaseConfig);
        catalog = new StoredClassCatalog(catalogDao);
    }

    private void autoconfig(String directory) {
        //config environment
        environmentConfig = new EnvironmentConfig();
        environmentConfig.setTransactional(true);
        environmentConfig.setAllowCreate(true);
        this.environment = new Environment(new File(directory), environmentConfig);

        //config database
        databaseConfig = new DatabaseConfig();
        databaseConfig.setTransactional(true);
        databaseConfig.setAllowCreate(true);
    }

    //Return the count of fetched documents
    @Override
    public int getCorpusSize() {
        synchronized (docDao) {
            return docTable.size();
        }
    }

    /**
     * CRUD operation for document table
     */
    @Override
    public String addDocument(String payload, String docType) {
        synchronized (docDao) {
            String key = URLParser.parsePayloadToMD5(payload);
            DocumentEntity documentEntity = new DocumentEntity(key, payload, docType);
            docTable.put(key, documentEntity);
            return key;
        }
    }

    @Override
    public String getDocument(String url) {
        synchronized (urlDao) {
            synchronized (docDao) {
                URLEntity urlEntity = urlTable.getOrDefault(url, null);
                if (urlEntity == null) {
                    return null;
                }
                DocumentEntity documentEntity = docTable.getOrDefault(urlEntity.getId(), null);
                String res = documentEntity == null ? null : documentEntity.getContent();
                return res;
            }
        }
    }

    @Override
    public DocumentEntity getDocumentEntity(String url) {
        synchronized (urlDao) {
            synchronized (docDao) {
                URLEntity urlEntity = urlTable.getOrDefault(url, null);
                if (urlEntity == null) {
                    return null;
                }
                DocumentEntity documentEntity = docTable.getOrDefault(urlEntity.getId(), null);
                return documentEntity;
            }
        }
    }

    @Override
    public boolean containsDocument(String document) {
        synchronized (docDao) {
            String key = URLParser.parsePayloadToMD5(document);
            return docTable.containsKey(key);
        }
    }

    /**
     * CRUD operation for user table
     */
    @Override
    public int addUser(String username, String password) {
        synchronized (userDao) {
            logger.info("Adding new user: {}", username);
            if (userTable.containsKey(username)) {
                logger.error("The username already exists. Username:{}", username);
                return -1;
            } else {
                logger.info("Adding new user. Username: {}", username);
                userTable.put(username, new UserEntity(username, password));
            }
        }
        return 0;
    }

    @Override
    public UserEntity getUser(String username) {
        synchronized (urlDao) {
            return userTable.getOrDefault(username, null);
        }
    }

    @Override
    public boolean getSessionForUser(String username, String password) {
        synchronized (userDao) {
            if (!userTable.containsKey(username)) {
                logger.info("The user doesn't exist.");
                return false;
            }
            UserEntity userEntity = userTable.get(username);
            MessageDigest messageDigest = null;
            try {
                messageDigest = MessageDigest.getInstance("SHA-256");
                password = new String(messageDigest.digest(password.getBytes()));
            } catch (Exception e) {
                logger.error("Exception when during decryption.");
            }
            return password.equals(userEntity.getPassword());
        }
    }

    /**
     * Gently terminate all the databases.
     */
    @Override
    public void close() {
        logger.info("Closing the databases.");
        if (catalogDao != null) {
            catalogDao.close();
        }
        if (docDao != null) {
            docDao.close();
        }
        if (userDao != null) {
            userDao.close();
        }
        if (urlDao != null) {
            urlDao.close();
        }
    }

    @Override
    public URLEntity getURL(URLInfo urlInfo) {
        synchronized (urlDao) {
            String url = URLParser.parseURL(urlInfo, true);
            logger.info("Getting URL:{}", url);
            return urlTable.getOrDefault(url, null);
        }
    }

    @Override
    public URLEntity removeURL(URLInfo urlInfo) {
        synchronized (urlDao) {
            String url = URLParser.parseURL(urlInfo, true);
            logger.info("Removing URL:{}", url);
            URLEntity entity = urlTable.remove(url);
            return entity;
        }
    }

    /**
     * Adding URL to the given target destination.
     */
    @Override
    public void addURL(URLEntity urlEntity) {
        synchronized (urlDao) {
            logger.info("Adding URL: {}", urlEntity.getUrl());
            urlTable.put(urlEntity.getUrl(), urlEntity);
        }
    }

    /**
     * Check if the target file is a html of not.
     * Return false if the target doesn't exist or is not a HTMl file.
     */
    @Override
    public boolean isHTML(String url) {
        synchronized (docDao) {
            synchronized (urlDao) {
                URLEntity urlEntity = urlTable.get(url);
                if (urlEntity == null) {
                    return false;
                }
                DocumentEntity documentEntity = docTable.get(urlEntity.getId());
                return documentEntity == null ? false : documentEntity.getDocType().equals("text/html");

            }
        }
    }

}

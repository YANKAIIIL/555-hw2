package edu.upenn.cis.cis455.storage;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.data.DocumentEntity;
import edu.upenn.cis.cis455.data.URLEntity;
import edu.upenn.cis.cis455.data.UserEntity;

public interface StorageInterface {

    /**
     * How many documents so far?
     */
    public int getCorpusSize();

    /**
     * Add a new document, getting its ID
     */
    public String addDocument(String payload, String docType);

    /**
     * Retrieves a document's contents by URL
     */
    public String getDocument(String url);

    public DocumentEntity getDocumentEntity(String url);

    public boolean containsDocument(String document);

    public boolean isHTML(String url);

    /**
     * Adds a user and returns an ID
     */
    public int addUser(String username, String password);

    /**
     * Tries to log in the user, or else throws a HaltException
     */
    public boolean getSessionForUser(String username, String password);

    public UserEntity getUser(String username);

    /**
     * Shuts down / flushes / closes the storage system
     */
    public void close();

    public URLEntity getURL(URLInfo urlInfo);

    public URLEntity removeURL(URLInfo urlInfo);

    public void addURL(URLEntity urlEntity);

}

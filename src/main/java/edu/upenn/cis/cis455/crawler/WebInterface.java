package edu.upenn.cis.cis455.crawler;

import edu.upenn.cis.cis455.crawler.handlers.LoginFilter;
import edu.upenn.cis.cis455.crawler.handlers.LoginHandler;
import edu.upenn.cis.cis455.crawler.handlers.LookupHandler;
import edu.upenn.cis.cis455.crawler.handlers.RegistrationHandler;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.Session;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import static spark.Spark.*;

public class WebInterface {
    private static Logger logger = LogManager.getLogger(WebInterface.class);

    public static void main(String args[]) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        if (args.length < 1 || args.length > 2) {
            System.out.println("Syntax: WebInterface {path} {root}");
            System.exit(1);
        }

        logger.info("Input is:{}", Arrays.toString(args));

        if (!Files.exists(Paths.get(args[0]))) {
            try {
                Files.createDirectory(Paths.get(args[0]));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        port(45555);
        StorageInterface database = StorageFactory.getDatabaseInstance(args[0]);

        LoginFilter testIfLoggedIn = new LoginFilter(database);

        if (args.length == 2) {
            staticFiles.externalLocation(args[1]);
            staticFileLocation(args[1]);
        }


        /**
         * Registering multiple filters and handlers for different pages.
         */
        before("/*", "*/*", testIfLoggedIn);
        // TODO:  add /register, /logout, /index.html, /, /lookup

        post("/login", new LoginHandler(database));
        post("/register", new RegistrationHandler(database));

        get("/logout", (req, res) -> {
//            req.session(false).invalidate();
//            res.redirect("index.html");
//            return "";
            Session session = req.session();
            session.invalidate();
            res.redirect("index.html");
            return "";
        });

        get("/lookup", new LookupHandler(database));
        awaitInitialization();
    }
}

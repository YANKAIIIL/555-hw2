package edu.upenn.cis.cis455.crawler.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Create a robot filter on the first seen of a website.
 * Allows and disallows are stored within the white list and blacklist.
 * Need to examine whether the page is ready for crawling or has been crawled recently.
 * Need to check whether the robot file exists or not first.
 */
public class RobotFilter {
    private static Logger logger = LogManager.getLogger(RobotFilter.class);
    String url;
    private boolean webReady;
    private long lastVisitedTimestamp;

    private int delay;
    private Set<String> whiteList;
    private Set<String> blackList;
    private Set<String> visited;


    public RobotFilter(String url) {
        this.url = url;
        whiteList = new HashSet<>();
        blackList = new HashSet<>();
        visited = new HashSet<>();

        logger.info("Processing robot.txt for site: {}", url);
        int status = robotFile(url);
        webReady = true;
        if (status == -1) {
            return;
        }
        webReady = true;
        lastVisitedTimestamp = System.currentTimeMillis();

    }

    /**
     * Crawled recently. Need to wait.
     */
    public boolean toWait() {
        if (System.currentTimeMillis() - lastVisitedTimestamp < delay * 1000 || !webReady) {
            return true;
        }
        lastVisitedTimestamp = System.currentTimeMillis();
        return false;
    }

    /**
     * Page is ready for crawling
     */
    public boolean canCrawl() {
        return this.webReady;
    }

    /**
     * The scanned path is allowed to parse or not.
     */
    public boolean canParse(String filePath) {
        if (!webReady) {
            return false;
        }
        filePath = Paths.get(filePath).normalize().toString();
        if (visited.contains(filePath)) {
            return false;
        }
        visited.add(filePath);
        int score = search(whiteList, blackList, filePath);
        return score >= 0;
    }

    //white == -1   not specified
    //white > black
    private int search(Set<String> whiteList, Set<String> blackList, String filePath) {
        int white = match(whiteList, filePath);
        int black = match(blackList, filePath);
        if (white > black) {
            return 1;
        } else if (white < black) {
            return -1;
        } else if (white == -1) {
            return 0;
        }
        return 1;
    }

    private int match(Set<String> list, String filepath) {
        int res = -1;
        for (String path : list) {
            int temp = checkMatch(path, filepath);
            res = Math.max(res, temp);
        }
        return res;
    }

    private int checkMatch(String path, String filepath) {
        if (compare(0, 0, path, filepath)) {
            return path.length();
        }
        return -1;
    }

    /**
     * Compare the crawling path with the list in a recursive way to complex path.
     */
    private boolean compare(int i, int j, String path, String filepath) {
        if (i == path.length()) return true;
        if (j == filepath.length()) {
            return i == path.length() - 1 && path.charAt(i) == '$';
        }
        if (path.charAt(i) == '*') {
            return compare(i + 1, j, path, filepath) || compare(i, j + 1, path, filepath);
        }
        if (path.charAt(i) != filepath.charAt(j)) {
            return false;
        }
        return compare(i + 1, j + 1, path, filepath);
    }

    /**
     * Check whether the robots.txt exists of not for the given url.
     * If exists, open a new socket and read the txt file line by line.
     */
    private int robotFile(String url) {
        //return -1 when failed to process robotFile
        String robotURL = url + "/robots.txt";
        InputStream inputStream = null;

        try {
            inputStream = openInputStream(robotURL);
        } catch (IOException e) {
            logger.error("Cannot read from URL: {}.", url);
        }
        if (inputStream == null) {
            logger.info("400/500 code whe processing robots.txt at URL: {}", url);
            return -1;
        }

        /**
         * Read line by line and handle the exception.
         */
        String line;
        StringBuffer responseBuffer = new StringBuffer();
        try (BufferedReader input = new BufferedReader(new InputStreamReader(inputStream))) {
            while ((line = input.readLine()) != null) {
                responseBuffer.append(line + "\n");
            }
        } catch (IOException e) {
            logger.error("Failed to read from socket.");
        }
        String response = responseBuffer.toString();
        /**
         * Process both cis455crawler and * to comply with the autograder and qa environment
         */
        processWithUserAgent(response, "cis455crawler");
        processWithUserAgent(response, "*");//For general purpose
        return 0;
    }

    /**
     * Break down the response with user-agent to get specific instructions.
     * We only need to process the cases that user-agent is * or cis455crawler.
     */
    private void processWithUserAgent(String response, String userAgent) {
        response = response.toLowerCase();
        String[] temp = response.split("user-agent:");
        List<String> input = new ArrayList<>();
        for (String s : temp) {
            input.add(s.trim());
        }
        /**
         * We only need to process 3 different kinds of entries.
         * crawl-delay, allow and disallow
         * For each of them, adding to the corresponding list.
         * All the entries should be in <key:value>
         */
        input.forEach(e -> {
            if (e.startsWith(userAgent + "\n") || e.startsWith(userAgent + "\r\n")) {
                String[] rows = e.split("(\r\n|\n)");
                for (String row : rows) {
                    String[] token = row.split(":");
                    if (token == null || token.length != 2) {
                        continue;
                    }
                    String key = token[0].toLowerCase().trim();
                    String value = token[1].toLowerCase().trim();
                    switch (key) {
                        case "crawl-delay":
                            this.delay = Integer.parseInt(value);
                            break;
                        case "allow":
                            String allowPath = Paths.get(value).normalize().toString();
                            whiteList.add(allowPath.endsWith("*") ? allowPath.substring(0, allowPath.length() - 1) : allowPath);
                            break;
                        case "disallow":
                            String disallowPath = Paths.get(value).normalize().toString();
                            blackList.add(disallowPath.endsWith("*") ? disallowPath.substring(0, disallowPath.length() - 1) : disallowPath);
                            break;
                    }
                }
            }
        });
    }

    /**
     * Implementation of opening new input stream
     * If code 200, open
     * If code 300-level, jump to another page
     * If code 404, it means the given site doesn't exist a robot.txt. There is no rules
     * for crawling.
     * If code other, return error.
     */
    private InputStream openInputStream(String inputURL) throws IOException {
        //Return null when get 400/500 level code
        URL url = new URL(inputURL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("User-Agent", "cis455crawler");
        connection.connect();
        int status = connection.getResponseCode();
        switch (status) {
            case HttpURLConnection.HTTP_OK:
                logger.info("Found robots.txt at URL: {}", inputURL);
                return connection.getInputStream();
            case HttpURLConnection.HTTP_MOVED_PERM:
            case HttpURLConnection.HTTP_MOVED_TEMP:
                String redirect = connection.getHeaderField("Location");
                logger.info("Redirect to {} while trying to find robots.txt", redirect);
                return openInputStream(redirect);
            case HttpURLConnection.HTTP_NOT_FOUND:
                logger.info("Not able to find robots.txt");
                webReady = true;
            default:
                logger.info("Error when attempt to find robots.txt. Error code: {}.", status);
        }
        return null;
    }

    public static void main(String[] args) {
        RobotFilter filter = new RobotFilter("https://google.com");
    }
}

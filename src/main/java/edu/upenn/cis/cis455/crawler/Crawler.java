package edu.upenn.cis.cis455.crawler;

import edu.upenn.cis.cis455.crawler.utils.RobotFilter;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.crawler.utils.URLParser;
import edu.upenn.cis.cis455.enums.Constant;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Crawler implementation class.
 */
public class Crawler implements CrawlMaster {
    private static Logger logger = LogManager.getLogger(Crawler.class);
    ///// TODO: you'll need to flesh all of this out. You'll need to build a thread
    // pool of CrawlerWorkers etc.

    static final int NUM_WORKERS = 10;

    /**
     * Max size and count of documents
     */
    private static int _size;
    private static int _count;

    /**
     * Setup the thread pool, db, blocking queue and etc.
     */
    private BlockingQueue<String> queue;
    private List<Worker> workers;
    private List<Thread> threadPool;
    private StorageInterface _db;

    AtomicInteger exitedWorkerCount;
    AtomicInteger documentCount;
    AtomicInteger busyWorker;

    private Set<String> signatures;
    //To the domain, exact file location is not included
    private Map<String, RobotFilter> robotMap;

    /**
     * Constructor. Start the thread pool and all the preparations.
     * Workers are encapsulated by the thread class. So we need to store the references of both of them.
     */
    public Crawler(String startUrl, StorageInterface db, int size, int count) {
        //size: Size sum of all the files. count: Count of all the files
        this._db = db;
        this._size = size;
        this._count = count;
        logger.info("Crawler initiation. Size:{}. Count:{}", _size, _count);

        queue = new LinkedBlockingQueue<>();
        try {
            queue.put(startUrl);
        } catch (InterruptedException e) {
            logger.error("Failed to initiate the blocking queue.");
            e.printStackTrace();
        }
        threadPool = new ArrayList<>();
        workers = new ArrayList<>();
        //Start thread pool
        for (int i = 0; i < NUM_WORKERS; i++) {
            Worker worker = new Worker(this, this.queue, this._db);
            workers.add(worker);
            Thread thread = new Thread(worker);
            thread.setName("Thread " + i);
            threadPool.add(thread);
        }
        documentCount = new AtomicInteger(0);
        busyWorker = new AtomicInteger(0);
        exitedWorkerCount = new AtomicInteger(0);
        signatures = new HashSet<>();
        robotMap = new HashMap<>();
    }

    /**
     * The given url is ready for future crawl
     */
    public boolean canCrawl(String url) {
        if (!robotMap.containsKey(url)) {
            synchronized (robotMap) {
                if (!robotMap.containsKey(url)) {
                    robotMap.put(url, new RobotFilter(url));
                }
            }
        }
        return robotMap.get(url).canCrawl();
    }

    /**
     * ready to parse the document at the given url
     */
    public boolean canParse(URLInfo url) {
        String key = URLParser.parseURL(url, false);
        return robotMap.get(key).canParse(url.getFilePath());
    }

    public boolean waitForCrawl(String url) {
        RobotFilter filter = robotMap.get(url);
        if (filter == null) return false;
        if (busyWorker.get() + documentCount.get() > this._count) return true;
        return filter.toWait();
    }

    /**
     * Test if the target is a valid document or not by its content-length and file type.
     */
    public boolean isValidDocument(int length, String type) {
        type = type.toLowerCase();
        if (type == null || length > this._size) {
            logger.error("Invalid document with length:{} and type:{}", length, type);
            return false;
        }
        boolean condition1 = type.equals(Constant.APPLICATION_XML.value());
        boolean condition2 = type.equals(Constant.TEXT_HTML.value());
        boolean condition3 = type.equals(Constant.TEXT_XML.value());
        boolean condition4 = type.endsWith(Constant.RSS_END.value());
        return condition1 || condition2 || condition3 || condition4;
    }


    /**
     * Main thread
     */
    public void start() {
        threadPool.forEach(e -> e.start());
        while (exitedWorkerCount.get() != NUM_WORKERS) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                logger.info("Crawler main thread interrupted unexpectedly.");
            }
        }
        threadPool.forEach(e -> {
            try {
                e.join();
            } catch (InterruptedException ex) {
                logger.info("Crawler worker thread interrupted unexpectedly.");
            }
        });
        logger.info("Crawler about to exit gracefully. New doc crawled: {}", documentCount.get());
    }

    /**
     * We've indexed another document
     */
    @Override
    public void incCount() {
        documentCount.getAndIncrement();
    }

    /**
     * Workers can poll this to see if they should exit, ie the crawl is done
     */
    @Override
    public boolean isDone() {
        return (documentCount.get() > this._count) || (queue.isEmpty() && busyWorker.get() == 0);
    }

    /**
     * Workers should notify when they are processing an URL
     */
    @Override
    public void setWorking(boolean working) {
        if (working) {
            busyWorker.getAndIncrement();
        } else {
            busyWorker.getAndDecrement();
        }
    }

    /**
     * Workers should call this when they exit, so the master knows when it can shut
     * down
     */
    @Override
    public void notifyThreadExited() {
        exitedWorkerCount.getAndIncrement();
        logger.info("Capture worker exit. Exit count: {}", exitedWorkerCount.get());
    }

    @Override
    public boolean needIndex(String payload) {
        String signature = URLParser.parsePayloadToMD5(payload);
        if (signatures.contains(signature)) {
            return false;
        }
        signatures.add(signature);
        return true;
    }

    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     */
    public static void main(String args[]) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]) * 1024 * 1024;
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;

        if (!Files.exists(Paths.get(args[1]))) {
            try {
                Files.createDirectory(Paths.get(args[1]));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);

        Crawler crawler = new Crawler(startUrl, db, size, count);

        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        crawler.start();

        while (!crawler.isDone())
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        // TODO: final shutdown
        db.close();
        System.out.println("Done crawling!");
    }

    public int getMaxSize() {
        return _size;
    }
}

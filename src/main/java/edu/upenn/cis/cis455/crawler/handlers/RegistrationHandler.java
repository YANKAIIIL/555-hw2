package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.enums.Page;
import edu.upenn.cis.cis455.storage.StorageInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.*;

import java.security.MessageDigest;

/**
 * Handling the registration behavior.
 * If invalid, redirect to the error page.
 * Store the password with SHA-256 algorithms.
 */
public class RegistrationHandler implements Route {
    private static Logger logger = LogManager.getLogger(RegistrationHandler.class);
    StorageInterface db;

    public RegistrationHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        logger.info("Request payload: {}", request.toString());
        QueryParamsMap params = request.queryMap();
        String username = params.value("username");
        String password = params.value("password");
        if (username == null || username.length() == 0 || password == null || password.length() == 0) {
            response.redirect(Page.REGISTER_ALL_FIELDS.value());
            logger.info("Invalid register request with username:{}. Redirect to {}",
                    username, Page.REGISTER_ALL_FIELDS.value());
        }
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (Exception e) {
            logger.error("Exception when during encryption.");
        }
        String encrypted = new String(messageDigest.digest(password.getBytes()));

        int status = db.addUser(username, encrypted);
        logger.info("Register new user: {} get status: {}", username, status);
        if (status == 0) {
//            response.redirect(Page.LOGIN_FORM.value());
            response.status(200);
            return "Registration succeed!";
        }
        return Spark.halt(409);
    }
}

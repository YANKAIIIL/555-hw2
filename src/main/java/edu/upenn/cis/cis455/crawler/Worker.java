package edu.upenn.cis.cis455.crawler;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.crawler.utils.URLParser;
import edu.upenn.cis.cis455.data.URLEntity;
import edu.upenn.cis.cis455.enums.Constant;
import edu.upenn.cis.cis455.storage.StorageInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import spark.Spark;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Worker implements Runnable {
    private static Logger logger = LogManager.getLogger(Worker.class);

    private Crawler _master;
    private BlockingQueue<String> _queue;
    private StorageInterface _db;
    private boolean working;

    public Worker(Crawler master, BlockingQueue<String> queue, StorageInterface db) {
        this._master = master;
        this._queue = queue;
        this._db = db;
    }

    @Override
    public void run() {
        try {
            while (true) {
                setWorking(false);
                /**
                 * nextURL is the link we are going to process in the next round.
                 */
                String nextURL = null;
                while (true) {
                    try {
                        nextURL = _queue.poll(1, TimeUnit.SECONDS);
                    } catch (InterruptedException e) {
                        logger.error("Unexpectedly interrupted when fetching from blocking queue. Thread: {}", Thread.currentThread().toString());
                    }
                    if (nextURL != null || this._master.isDone()) {
                        break;
                    }
                }
                if (nextURL == null) {
                    //Need to tell whether there is a new link or the master is closed
                    //If master is closed, all the workers can be closed.
                    break;
                }
                logger.info("Thread: {}, Raw URL: {}", Thread.currentThread().getName(), nextURL);
                setWorking(true);
                /**
                 Parse the URLInfo to links for different purpose
                 */
                URLInfo url = new URLInfo(nextURL);
                String parsedURLToDocument = URLParser.parseURL(url, true);
                String parsedURLToHost = URLParser.parseURL(url, false);

                /**
                 * Examine if we can crawl the page and if we need to waiting for the next crawl.
                 */
                if (!_master.canCrawl(parsedURLToHost)) {
                    continue;
                }

                while (_master.waitForCrawl(parsedURLToHost)) {
                    if (this._master.isDone()) {
                        logger.info("Master node is closed.");
                        break;
                    }
                }
                /**
                 * Check if the crawling process is finished or not.
                 */
                if (this._master.isDone()) {
                    //break if the master node is down
                    break;
                }
                if (!_master.canParse(url)) {
                    logger.info("Cannot parse the url:{}", url);
                    continue;
                }

                /**
                 * Setup the timestamp and payload for future steps.
                 */
                long lastTimestamp = 0;
                String payload = null;
                boolean isHtml = false;
                URLEntity urlEntity = _db.getURL(url);
                if (urlEntity != null) {
                    lastTimestamp = urlEntity.getTimestamp();
                }
                try {
                    //send head method
                    HttpURLConnection connection = connect(parsedURLToDocument, "HEAD");
                    connection.setIfModifiedSince(lastTimestamp);
                    int status = connection.getResponseCode();
                    /**
                     * If OK, we can process the document
                     * For different status code, process accordingly.
                     */
                    switch (status) {
                        case HttpURLConnection.HTTP_OK:
                            String type = connection.getContentType().split(";")[0].toLowerCase().trim();
                            if (!this._master.isValidDocument(connection.getContentLength(), type)) {
                                logger.info("Removing URL due to invalid document. Length: {}. Type: {}", connection.getContentLength(), type);
                                URLEntity removeURL = this._db.removeURL(url);
                                connection.disconnect();
                                continue;
                            }
                            connection.disconnect();
                            HttpURLConnection getConnection = connect(parsedURLToDocument, "GET");
                            logger.info(parsedURLToDocument + ":downloading");
                            if (getConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                                URLEntity removeURL = this._db.removeURL(url);
                                logger.error("Failed when downloading file from URL:{}", parsedURLToDocument);
                            }
                            payload = parseContent(getConnection);
                            isHtml = type.equals(Constant.TEXT_HTML.value());
                            if (!_db.containsDocument(payload)) {
                                logger.info("Crawled an unseen document");
                                this._master.incCount();
                            }
                            String documentId = _db.addDocument(payload, type);
                            _db.addURL(new URLEntity(parsedURLToDocument, documentId, connection.getLastModified()));
                            break;
                        case HttpURLConnection.HTTP_NOT_MODIFIED:
                            //not modified
                            logger.info(parsedURLToDocument + ":not modified");
                            payload = _db.getDocument(parsedURLToDocument);
                            isHtml = _db.isHTML(parsedURLToDocument);
                            break;
                        case HttpURLConnection.HTTP_MOVED_PERM:
                        case HttpURLConnection.HTTP_MOVED_TEMP:
                            //for 301 or 302, put the redirected link into the queue
                            logger.info("Removing URL due to redirection. URL:{}", url.toString());
                            URLEntity removeURL = this._db.removeURL(url);
                            String docId = removeURL.getId();
                            String mock = String.format("<a href=\"%s\"></a>", connection.getHeaderField("Location"));
                            Document mockHtml = Jsoup.parse(mock);
                            mockHtml.absUrl(parsedURLToDocument);
                            _queue.put(mockHtml.select("a").first().attr("abs:href"));
                            connection.disconnect();
                            continue;
                        default:
                            logger.error("Unexpected status code when sending HEAD request. URL:{}", parsedURLToDocument);
                            connection.disconnect();
                            continue;
                    }
                    connection.disconnect();
                } catch (IOException | InterruptedException e) {
                    logger.error("Unexpected exception:{}", e.toString());
                }
                if (!isHtml || !_master.needIndex(payload)) {
                    continue;
                }
                /**
                 * Parse all the links within the document and feed it to the blocking queue for
                 * the next round of crawling.
                 */
                Document document = Jsoup.parse(payload);
                document.setBaseUri(parsedURLToDocument);
                document.getElementsByAttribute("href").forEach(e -> {
                    String next = e.absUrl("href");
                    try {
                        logger.info("Adding new URL to the queue. URL: {}", next);
                        _queue.put(next);
                    } catch (InterruptedException interruptedException) {
                        logger.error("Error when adding new URL to the queue. URL: {}", next);
                    }
                });
            }
        } finally {
            if (working) {
                setWorking(false);
            }
            this._master.notifyThreadExited();
        }
    }

    /**
     * Parse the payload fetched from the connection.
     * Return it as a string
     */
    private String parseContent(HttpURLConnection connection) throws IOException {
        BufferedReader input = null;
        try {
            input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            int socketLength = connection.getContentLength();

            int size = socketLength == -1 ? this._master.getMaxSize() : socketLength;
            int read = 0;
            char[] readBuffer = new char[size];

            StringBuffer sb = new StringBuffer();
            String lines = null;
            while ((lines = input.readLine()) != null) {
                sb.append(lines + "\r\n");
            }
//            while(read != size){
//                int temp = input.read(readBuffer, read, size - read);
//                if(temp != -1){
//                    read += temp;
//                }else{
//                    break;
//                }
//                read += input.read(readBuffer, read, size - read);
//            }
            input.close();
//            String res = new String(readBuffer).trim();
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Connect to a target http destination
     * Need to set the user-agent to comply with the instructions
     */
    private HttpURLConnection connect(String rawUrl, String method) {
        URL url = null;
        HttpURLConnection conn = null;
        try {
            url = new URL(rawUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(method);
            conn.setRequestProperty("User-Agent", "cis455crawler");
            return conn;
        } catch (MalformedURLException e) {
            logger.info("Invalid URL: {}. Server internal error.", rawUrl);
            Spark.halt(500);
        } catch (IOException e) {
            logger.info("Socket exception.");
            Spark.halt(500);
        }
        return conn;
    }

    /**
     * Label the status of current thread
     */
    private synchronized void setWorking(boolean status) {
        if (this.working != status) {
            this._master.setWorking(status);
        }
        this.working = status;
    }
}

package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.crawler.utils.URLParser;
import edu.upenn.cis.cis455.data.DocumentEntity;
import edu.upenn.cis.cis455.storage.StorageInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

/**
 * Handle the lookup requests.
 * Read from the db to see if the url exists or not.
 * If exists, retrieve the document from db.
 * Otherwise, return 404.
 */
public class LookupHandler implements Route {
    private static Logger logger = LogManager.getLogger(LookupHandler.class);
    StorageInterface db;

    public LookupHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        //TODO
        logger.info("Request payload: {}", request.toString());
        String url = request.queryParams("url");
        if (url == null) {
            logger.error("Input URL is a null.");
            Spark.halt(404);
            response.status(404);
        }
        URLInfo urlInfo = new URLInfo(url);
        String documentPath = URLParser.parseURL(urlInfo, true);
        DocumentEntity documentEntity = db.getDocumentEntity(documentPath);
        /**
         * If the target document doesn't exist, return 404.
         */
        if (documentEntity == null) {
            logger.error("Not exist in db.");
            Spark.halt(404);
            response.status(404);
            return null;
        }
        /**
         * Set doc type and return
         */
        response.type(documentEntity.getDocType());
        return documentEntity.getContent();
    }

    private String restoreURL(URLInfo urlInfo) {
        StringBuffer sb = new StringBuffer();
        sb.append(urlInfo.isSecure() ? "https://" : "http://");
        sb.append(urlInfo.getHostName());
        sb.append(":" + urlInfo.getPortNo());
        return sb.toString();
    }
}

package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.*;

public class LoginHandler implements Route {
    StorageInterface db;

    public LoginHandler(StorageInterface db) {
        this.db = db;
    }

    /**
     * Handle login requests.
     * Return the username when success. Session expires in 300 seconds.
     * Otherwise, redirect to the login-form
     */
    @Override
    public String handle(Request req, Response resp) throws HaltException {
        String user = req.queryParams("username");
        String pass = req.queryParams("password");

        System.err.println("Login request for " + user + " and " + pass);
        if (db.getSessionForUser(user, pass)) {
            System.err.println("Logged in!");
            Session session = req.session();
            session.maxInactiveInterval(300);

            session.attribute("user", user);
            session.attribute("password", pass);
//            String payload = String.format("<!DOCTYPE html>\n" +
//                    "<html>\n" +
//                    "<head>\n" +
//                    "    <title>Index</title>\n" +
//                    "</head>\n" +
//                    "<body>\n" +
//                    "<h1>This is index.html</h1>\n" +
//                    "<h1>Welcome%s</h1>\n" +
//                    "</body>\n" +
//                    "</html>", user);
            resp.body(user);
            return user;
        } else {
            System.err.println("Invalid credentials");
            resp.redirect("/login-form.html");
        }

        return "";
    }
}

package edu.upenn.cis.cis455.crawler.utils;

import edu.upenn.cis.cis455.enums.Constant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.Spark;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Util class for parsing different kidns of stuff, including the file content.
 */
public class URLParser {
    private static Logger logger = LogManager.getLogger(URLParser.class);

    /**
     * Parsing the URL based on the URLInfo class.
     * If to File is true, we need to add the file path to the result.
     * Otherwise only add port, domain and protocol.
     * Using HTTPS and HTTP for different protocol.
     */
    public static String parseURL(URLInfo urlInfo, boolean toFile) {
        String hostName = urlInfo.getHostName();
        int port = urlInfo.getPortNo();
        String path = urlInfo.getFilePath();
        boolean isSecure = urlInfo.isSecure();

        StringBuffer sb = new StringBuffer();
        sb.append(isSecure ? Constant.HTTPS.value() : Constant.HTTP.value());
        sb.append(hostName);
        sb.append(String.format(":%s", port));
        sb.append(toFile ? "/" + path : "");//Format is -> "/path"
        return sb.toString().replaceAll("//", "/").replaceFirst("/", "//");
    }

    /**
     * Encode the payload with MD5 signature.
     * All the files will have a unique signature.
     */
    public static String parsePayloadToMD5(String payload) {
        MessageDigest messageDigest;
        String md5 = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            md5 = new String(messageDigest.digest(payload.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            logger.error("Failed to get MD5 instances. Server internal error.");
            Spark.halt(500);
        }
        return md5;
    }


}
